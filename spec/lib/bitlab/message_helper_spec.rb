# encoding: utf-8
require 'spec_helper'
require 'action_view'
require 'active_support'
require_relative '../../../app/helpers/message_helper'

include ActionView::Helpers
include ActionView::Context
include MessageHelper

describe MessageHelper, type: :helper do
  it 'should return a counter_box' do
    text1 = 'DEUDORES'
    text2 = 'TOTALES'
    expect(message_counter_box(50, text1, text1, text2).gsub(/\s/, '').downcase)
    .to eql(MESSAGE_COUNTER_BOX.gsub(/\s/, '').downcase)
  end

end

MESSAGE_COUNTER_BOX = <<-HTML
  <div class="panel panel-tile
  text-center br-a br-grey"><div class="panel-body"><h1 class="fs30 mt5 mbn">50
  </h1><h6 class="text-system">DEUDORES</h6></div><div class="panel-footer br-t
  p12"><span class="fs11"><i class="fa fa-user pr5"></i><span>DEUDORES</span><b>
  TOTALES</b></span></div></div>
HTML
