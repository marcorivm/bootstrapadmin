## Posibles Helpers


### Navbar Menu

Menu de barra superior de navegación izquierda

```html

<ul class="nav navbar-nav navbar-left">
  <li class="dropdown menu-merge hidden-xs">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown
      <span class="caret caret-tp"></span>
    </a>
    <ul class="dropdown-menu" role="menu">
      <li><a href="#">Action</a></li>
      <li><a href="#">Another action</a></li>
      <li><a href="#">Something else here</a></li>
      <li class="divider"></li>
      <li><a href="#">Separated link</a></li>
    </ul>
  </li>
  <li class="hidden-xs">
    <a class="request-fullscreen toggle-active" href="#">
    <span class="ad ad-screen-full fs18"></span></a>
  </li>
</ul>

```

Dropdown menu

```html

<li class="dropdown menu-merge hidden-xs">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown
    <span class="caret caret-tp"></span>
  </a>
  <ul class="dropdown-menu" role="menu">
    <li><a href="#">Something else here</a></li>
    <li class="divider"></li>
    <li><a href="#">Separated link</a></li>
  </ul>
</li>

```

Navbar Menu Link

```html

<li class="hidden-xs">
  <a class="request-fullscreen toggle-active" href="#">
  <span class="ad ad-screen-full fs18"></span></a>
</li>

```

Sidebar Menu

```html

<aside id="sidebar_left" class="nano nano-light affix has-scrollbar">

  <!-- Start: Sidebar Left Content -->
  <div class="sidebar-left-content nano-content" tabindex="0" style="margin-right: -15px;">


    <!-- End: Sidebar Header -->

    <!-- Start: Sidebar Menu -->
    <ul class="nav sidebar-menu">

    Sidebar Menu :: Label
   <li class="sidebar-label pt20">Menu</li>


  Sidebar Menu :: Link
  <li>
    <a href="pages_calendar.html">
      <span class="fa fa-calendar"></span>
      <span class="sidebar-title">Calendar</span>
      <span class="sidebar-title-tray">
        <span class="label label-xs bg-primary">New</span>
      </span>
    </a>
  </li>

  Sidebar Menu :: Accordion
  <li>
    <a class="accordion-toggle" href="#">
      <span class="fa fa-columns"></span>
      <span class="sidebar-title">Layout Templates</span>
      <span class="caret"></span>
    </a>    
    <ul class="nav sub-nav" style="">

    Sidebar Menu :: Accordion Link    
    <li>
      <a href="layout_sidebar-left-static.html">
        Left Static </a>
    </li>

  </li>
  </ul>


  Sidebar Menu :: Stats

  <li class="sidebar-stat">
    <a href="#projectOne" class="fs11">
      <span class="fa fa-dropbox text-warning"></span>
      <span class="sidebar-title text-muted">Bandwidth</span>
      <span class="pull-right mr20 text-muted">58%</span>
      <div class="progress progress-bar-xs mh20">
        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 58%">
          <span class="sr-only">58% Complete</span>
        </div>
      </div>
    </a>
  </li>

</ul>

    <!-- End: Sidebar Menu -->

    <!-- Start: Sidebar Collapse Button -->
    <div class="sidebar-toggle-mini">
      <a href="#">
        <span class="fa fa-sign-out"></span>
      </a>
    </div>
    <!-- End: Sidebar Collapse Button -->

  </div>
  <!-- End: Sidebar Left Content -->

  <div class="nano-pane">
    <div class="nano-slider" style="height: 210px; transform: translate(0px, 0px);"></div>
  </div>
</aside>


```


Sidebar Header Menu `No se que sea :'(`

```html
<!-- Start: Sidebar Header -->
<header class="sidebar-header">

<!-- Sidebar Widget - Menu (Slidedown) -->
<div class="sidebar-widget menu-widget">
<div class="row text-center mbn">
  <div class="col-xs-4">
    <a href="dashboard.html" class="text-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Dashboard">
      <span class="glyphicon glyphicon-home"></span>
    </a>
  </div>
  <div class="col-xs-4">
    <a href="pages_messages.html" class="text-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Messages">
      <span class="glyphicon glyphicon-inbox"></span>
    </a>
  </div>
  <div class="col-xs-4">
    <a href="pages_profile.html" class="text-alert" data-toggle="tooltip" data-placement="top" title="" data-original-title="Tasks">
      <span class="glyphicon glyphicon-bell"></span>
    </a>
  </div>
  <div class="col-xs-4">
    <a href="pages_timeline.html" class="text-system" data-toggle="tooltip" data-placement="top" title="" data-original-title="Activity">
      <span class="fa fa-desktop"></span>
    </a>
  </div>
  <div class="col-xs-4">
    <a href="pages_profile.html" class="text-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Settings">
      <span class="fa fa-gears"></span>
    </a>
  </div>
  <div class="col-xs-4">
    <a href="pages_gallery.html" class="text-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cron Jobs">
      <span class="fa fa-flask"></span>
    </a>
  </div>
</div>
</div>

<!-- Sidebar Widget - Author (hidden)  -->
<div class="sidebar-widget author-widget hidden">
<div class="media">
  <a class="media-left" href="#">
    <img src="/public/assets/img/avatars/3.jpg" class="img-responsive">
  </a>
  <div class="media-body">
    <div class="media-links">
       <a href="#" class="sidebar-menu-toggle">User Menu -</a> <a href="pages_login(alt).html">Logout</a>
    </div>
    <div class="media-author">Michael Richards</div>
  </div>
</div>
</div>

<!-- Sidebar Widget - Search (hidden) -->
<div class="sidebar-widget search-widget hidden">
<div class="input-group">
<span class="input-group-addon">
  <i class="fa fa-search"></i>
</span>
<input type="text" id="sidebar-search" class="form-control" placeholder="Search...">
</div>
</div>

</header>
```
