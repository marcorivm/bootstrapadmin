# Absolute Bootstrap Admin

Instalar las gemas:

```ruby

gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'bootstrap-admin', git: 'git@bitbucket.org:bitlabmx/bootstrapadmin.git', tag: 'v1.5'

```
Versiones disponibles:

 - version 1.5 tag: 'v1.5': Documentada y con helpers
 - version 1.4 tag: 'v1.4': Se agregaron helpers, branch developmente durante el 2015
 - version 1.0 tag: 'v1.0': Version master, del 2015

    bundle install

Para incluir los estaticos ejecutar

    rails generate bootstrap:install

Para generar un template ejecutar

    rails generate bootstrap:layout [LAYOUT_NAME]

## Core Plugins

 - [Bootstrap Mutiselect](https://github.com/davidstutz/bootstrap-multiselect)
 - [Moment.js](http://momentjs.com/)


## Plugins

 - animete
 - bstimeout
 - bstour
 - CKEditor
 - c3charts
 - countdown
 - colorpicker
 - datatable
 - datepicker
 - daterangepicker
 - duallistbox
 - gmaps
 - highcharts
 - jquerymask
 - maxlength
 - select2
 - typeahead

### [Animate](https://github.com/daneden/animate.css)

bootstap_admin.css

```css
 /*
  *= require bitlab/plugin/animate
  */
```

### [bstimeout](https://github.com/orangehill/bootstrap-session-timeout)

application.js

```javascript

//= require bitlab/plugin/bstimeout

```

### [bstour](http://bootstraptour.com/)

application.js

```javascript

//= require bitlab/plugin/bstour

```

bootstap_admin.css

```css

 /*
  *= require bitlab/plugin/bstour
  */

```

### [Countdown](http://keith-wood.name/countdown.html)

application.js

```javascript

//= requite bitlab/plugin/countdown

```

### [Cropper](http://fengyuanchen.github.io/cropper/)

application.js

```javascript

//require bitlab/plugin/cropper

```

bootstap_admin.css

```css

/*
 *= require bitlab/plugin/cropper
 */

```

### [Dropzone](http://www.dropzonejs.com)

application.js

```javascript

//= require bitlab/plugin/dropzone

```

bootstap_admin.css

```css

/*
 *= require bitlab/plugin/dropzone
 */

```

Ejemplo

```html
<form action="/file-upload" class="dropzone dropzone-sm" id="dropZone">
  <div class="fallback">
    <input name="file" type="file" multiple />
  </div>
</form>
```

### [Flip](https://nnattawat.github.io/flip/)

application.js

```javascript

//= require bitlab/plugin/flip

```

### [file-upload](#)

application.js

```javascript

//= require bitlab/plugin/fileupload

```

### [Fullcalendar](http://fullcalendar.io/docs/)

application.js

```javascript

//= require bitlab/plugin/fullcalendar
//= require bitlab/plugin/fullcalendar/i18n/(es|en)

```

Una opcione adicional es la posibilidad de integrar con google calendar, para hacer uso de esta opcion es necesario agregar el plugin `gcal`

```javascript

//= require bitlab/plugin/fullcalendar/plugin/gcal

```

bootstap_admin.css

```css

/*
 *= require bitlab/plugin/fullcalendar
 */

```

### [Footable](http://fooplugins.com/plugins/footable-jquery/)

application.js

```javascript

//= require bitlab/plugin/footable
//= require bitlab/plugin/footable/plugins/(fitler|bookmarkable|grid|paginate|sort|striping)

```

bootstap_admin.css

```css

/*
 *= require bitlab/plugin/footable
 *= require bitlab/plugin/footable/theme/metro
 */

```

### [ladda](http://lab.hakim.se/ladda/)

application.js

```javascript

//= require bitlab/plugin/ladda

```

bootstap_admin.css

```css

/*
 *= require bitlab/plugin/ladda
 */

```

### [Globalize](http://github.com/jquery/globalize)

application.js

```javascript

//= require bitlab/plugin/globalize

```

### [Holder](http://imsky.github.io/holder)

application.js

```javascript

//= require bitlab/plugin/holder

```

### [ImageZoom](www.elevateweb.co.uk/image-zoom)

application.js

```javascript

//= require bitlab/plugin/imagezoom

```

### [jQuerySpin](http://fgnass.github.io/spin.js/)

application.js

```javascript

//= require bitlab/plugin/jquery.spin

```

### [jQueryDial](https://github.com/aterrien/jQuery-Knob)

```javascript

//= require bitlab/plugin/jquerydial

```

### [ladda](http://lab.hakim.se/ladda)

```javascript

//= require bitlab/plugin/ladda

```

```css
/*
 *=require bitlab/plugin/ladda
 */

```

### [lazyline](https://github.com/camoconnell/lazy-line-painter)

```javascript

//= require bitlab/plugin/lazyline

```

### [magnific](http://dimsemenov.com/plugins/magnific-popup/)

```javascript

//= require bitlab/plugin/magnific

```

### [Bootstrap-Markdown](http://www.codingdrama.com/bootstrap-markdown/)

```javascript

//= require bitlab/plugin/bootstrap-markdown

```
Por omision bootstrap-markdown se encuentra con el idioma ingles, si deseas utilizarlos en expañol, agrega la siguiente linea:

```javascript

//= require bitlab/plugin/bootstrap-markdown/i18n/es

```

### [Mixitup](https://mixitup.kunkalabs.com/)

```javascript

//= require bitlab/plugin/mixitup

```

### [Nestable](http://dbushell.github.io/Nestable/)

```javascript

//= require bitlab/plugin/nestable

```

```css

/*
 *= require bitlab/plugin/nestable
 */

```

### [nprogress](http://ricostacruz.com/nprogress)

[Nestable-Github](https://github.com/rstacruz/nprogress)

```javascript

//= require bitlab/plugin/nprogress

```

Para utilizar con turbolinks agrega os siguientes eventos :grin:

```javascript

$(document).on('page:fetch',   function() { NProgress.start(); });
$(document).on('page:change',  function() { NProgress.done(); });
$(document).on('page:restore', function() { NProgress.remove(); });

```

### [pnotify](https://sciactive.github.io/pnotify/)

```javascript

//= require bitlab/plugin/pnotify

```

### [slick](http://kenwheeler.github.io/slick/)

application.js

```javascript

//= require bitlab/plugin/slick

```

bootstap_admin.css

```css

 /*
  *= require bitlab/plugin/slick
  */

```

### [summernote](http://summernote.org/#/)

application.js

```javascript

//= require bitlab/plugin/summernote

```

bootstap_admin.css

```css

 /*
  *= require bitlab/plugin/summernote
  */

```

#### [bootstrap-tabdrop](http://www.eyecon.ro/bootstrap-tabdrop/)

application.js

```javascript

//= require bitlab/plugin/bootstrap-tabdrop

```

### [tagmanager](https://maxfavilli.com/jquery-tag-manager)

application.js

```javascript

//= require bitlab/plugin/tagmanager

```

bootstap_admin.css

```css

 /*
  *= require bitlab/plugin/tagmanager
  */

```

### [tagsinput](http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/)

application.js

```javascript

//= require bitlab/plugin/tagsinput

```

bootstap_admin.css

```css

 /*
  *= require bitlab/plugin/tagsinput
  */

```

### [waypoints](https://github.com/imakewebthings/waypoints)

application.js

```javascript

//= require bitlab/plugin/waypoints

```

### [x-editable](https://vitalets.github.io/x-editable/)

application.js

```javascript

//= require bitlab/plugin/x-editable
//= require bitlab/plugin/x-editableplugins/(address|typeahead|wysihtml5)

```

bootstap_admin.css

```css

 /*
  *= require bitlab/plugin/x-editable
  */

```


### [CKEditor](http://docs.ckeditor.com/#!/guide/dev_installation)

##### Instalacion basica

application.js

```javascript

//= require bitlab/plugin/ckeditor

```

bootstap_admin.css

```css

 /*
  *= require bitlab/plugin/bstour
  */

```


##### Instalacion de plugins

###### wsc

application.js

```javascript

//= require bitlab/plugin/(wsc|wsc_ie)

```

bootstap_admin.css

```css

 /*
  *= require bitlab/plugin/wsc
  */

```

###### scayt

application.js

```javascript

//= require bitlab/plugin/scayt

```

bootstap_admin.css

```css

 /*
  *= require bitlab/plugin/scayt
  */

```

###### a11help

application.js

```javascript

//= require bitlab/plugin/a11yhelp
//= require bitlab/plugin/a11yhelp/i18n/(en|es)

```


###### specialchar

application.js

```javascript

//= require bitlab/plugin/specialchar
//= require bitlab/plugin/specialchar/i18n/(en|es)

```

###### specialchar

application.js

```javascript

//= require bitlab/plugin/specialchar
//= require bitlab/plugin/specialchar/i18n/(en|es)

```

###### about

application.js

```javascript

//= require bitlab/plugin/about

```

###### clipboard

application.js

```javascript

//= require bitlab/plugin/clipboard

```

###### image

application.js

```javascript

//= require bitlab/plugin/image

```

###### link

application.js

```javascript

//= require bitlab/plugin/link

```

###### pastefromword

application.js

```javascript

//= require bitlab/plugin/pastefromword

```

###### table

application.js

```javascript

//= require bitlab/plugin/table

```

###### tabletools

application.js

```javascript

//= require bitlab/plugin/tabletools

```

###### specialchart

application.js

```javascript

//= require bitlab/plugin/specialchart

```

> Pendiente redacatar configuración de plugins especiales (scayt, wsc)


### [DataTable](https://www.datatables.net/)


application.js

```javascript

//= require bitlab/plugin/datatable

```

bootstap_admin.css

```css
 /*
  *= require bitlab/plugin/datatable/default
  */
```

Ó

```css
 /*
  *= require bitlab/plugin/datatable/original
  */
```

Para hacer uso del tema `default` de DataTable deben se compilarse los assets previamente para mantener accesibles las imagines que se utilizan en este tema.

### [Google Maps](https://developers.google.com/maps/documentation/javascript/)

application.js

```javascript

//= require bitlab/plugin/gmaps

```

bootstap_admin.css

```css
 /*
  *= require bitlab/plugin/gmaps
  */
```

### [Highcharts](http://www.highcharts.com/)

application.js

```javascript

//= require bitlab/plugin/highcharts

```

### [C3Charts](http://c3js.org/)

application.js

```javascript

//= require bitlab/plugin/c3charts

```

bootstap_admin.css

```css
 /*
  *= require bitlab/plugin/c3charts
  */
```

### [Colorpicker](http://mjolnic.com/bootstrap-colorpicker/)

application.js

```javascript

//= require bitlab/plugin/colorpicker

```

bootstap_admin.css

```css
 /*
  *= require bitlab/plugin/colorpicker
  */
```

> Hay que poner las imagenes disponibles en el servidor de las fuentes

### [DatePicker](https://github.com/Eonasdan/bootstrap-datetimepicker)

application.js

```javascript

//= require bitlab/plugin/datepicker

```

bootstap_admin.css

```css
 /*
  *= require bitlab/plugin/datepicker
  */
```
### [DateRangePicker](http://www.daterangepicker.com/)

application.js

```javascript

//= require bitlab/plugin/daterangepicker

```

bootstap_admin.css

```css
 /*
  *= require bitlab/plugin/daterangepicker
  */
```

### [Select2](https://github.com/select2/select2)

application.js

```javascript

//= require bitlab/plugin/select2
//= require bitlab/plugin/select2/i18n/(en|es)

```

bootstap_admin.css

```css
 /*
  *= require bitlab/plugin/daterangepicker
  *= require bitlab/plugin/select2/theme/(default|classic)
  */
```

### [Duallistbox](http://www.virtuosoft.eu/code/bootstrap-duallistbox/)

application.js

```javascript

//= require bitlab/plugin/duallistbox

```

### [jQueryMask](http://digitalbush.com/projects/masked-input-plugin/)

application.js

```javascript

//= require bitlab/plugin/jquerymask

```

### [MaxLength](https://github.com/mimo84/bootstrap-maxlength/)

application.js

```javascript

//= require bitlab/plugin/maxlength

```

## [typeahead.js](https://github.com/twitter/typeahead.js)

```javascript

//= require bitlab/plugin/typeahead

```

### Helpers


## Navbar Helpers

  - navbar_divider
  - navbar_dropdown `block`
  - navbar_dropdown_footer
  - navbar_link˚

```rhtml
<%=  navbar_dropdown 'Dropdown Title' do %>

  <%= navbar_dropdown_footer 'Title', 'path', 'fa fa-power-off' %>

<% end %>
```

```rhtml

<%= navbar_divider %>

```

```rhtml

<%= navbar_link 'Title', 'path', 'fa fa-cart-plus' %>

```
## Form Helpers

  - section_divider
  - chat helpers
    - chat_content
    - chat_message
  - absolute_simple_field (`validando`)
  - absolute_custom_field (`validando`)
  - form_image_content (`validando`)
  - form_input_content (`validando`)

```rhtml
<%= chat_content do %>
  <% @messages.find_each do |message| %>
  <%= chat_message(message.profile, message.user_name, message.message, message.created_at, message.attachment)  %>
  <% end %>
<% end %>
```

- profile: Path o url de imagen de perfile
- user_name: nombre de usuario
- message: cuerpo del mensaje
- created_at: fecha de creacion del mensaje
- attachment: imagen adjunta a un mensaje (en caso de tenerla), por defecto es `nil`


```rhtml

<%= section_divider 'Title' %>

```

## Message Helpers

- message_counter_box

```rhtml

<%= message_counter_box '1,426', 'New orders', '3% INCREASE', '1W AGO' %>

```

## Sidebar Menu Helpers

- sidebar
- sidebar_toggle
- sidebar_image_accordion
  - accordion_image_link
- sidebar_icon_accordion
  - accordion_icon_link
- sidebar_image_link
- sidebar_icon_link


```rhtml

<%= sidebar_icon_accordion(t('traffic'), "fa fa-users", css) do %>
<% end %>

```

```rhtml

<%= sidebar_image_accordion(t('traffic'), "traffic.png", css) do %>
<% end %>

```
- css: es un hash con clases de css que seran agregadas a cada componente.

Las clases de css que pueden ser agregadas son las siguientes:

```ruby

  { li: { id: '', class: '' }, a: { id: '', class: '' } }

```

```rhtml

<%= accordion_icon_link(t('traffic'), "#",
        "fa fa-users", nil, css) %>

```

```rhtml

<%= accordion_image_link(t('traffic'), "#",
        "traffic.png", nil, css) %>

```
- css: es un hash con clases de css que seran agregadas a cada componente.

Las clases de css que pueden ser agregadas son las siguientes:

```ruby

  { li: {class: '', id: ''}, a: {id: '', class: ''} }

```

```rhtml

  <%= sidebar_image_link('title', 'image.png', 'path', 'label', css) %>

```

```rhtml

  <%= sidebar_icon_link('title', 'image.png', 'path', 'label', css) %>

```

- css: recibe un string que se asignara como clase al li del link, por defecto es `nil`

### Ejemplo de sidebar menu
```rhtml

<%= sidebar color: 'sidebar-light' do %>
  <%= render '/layouts/sidebar/header' %>
  <%= sidebar_menu do %>
    <li class="sidebar-label pt20">Menu</li>
    <%= sidebar_icon_accordion 'Example', 'fa fa-home' do  %>
      <%= accordion_icon_link 'Link 1', 'path', nil, nil %>
      <%= accordion_icon_link 'Link 2', 'path', nil, nil %>
      <%= accordion_icon_link 'Link 3', 'path', nil, nil %>
    <% end %>
    <%= sidebar_toggle %>
  <% end %>
<% end %>

```

- atributos de sidebar
  - color: es una clase que define el color, por default absolute admin template tiene las siguientes clases:
    - sidebar-default
    - sidebar-light
    - sidebar-light light

## Generic List Helpers

> Uso en app/views/layouts/_generic_list.html.erb
