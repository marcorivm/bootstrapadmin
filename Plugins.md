# Lista de plugins y su estatus

## Pendientes

 - bitlab/plugins/canvasbg
 - bitlab/plugins/fancytree
 - bitlab/plugins/highlight
 - bitlab/plugins/jvectormap
 - bitlab/plugins/mapplic
 - bitlab/plugins/markitup
 - bitlab/plugins/oggrid
 - bitlab/plugins/validate

## Funcionalidad pendiente de verificar

 - bitlab/plugins/c3charts `Pendiente`

## Funcionalidad verificada

- bitlab/plugins/colorpicker `OK`
- bitlab/plugins/datatables `OK`
- bitlab/plugins/datepicker `OK`
 - bitlab/plugins/countdown `OK`
- bitlab/plugins/daterange `OK`
- bitlab/plugins/duallistbox `OK`
- bitlab/plugins/highcharts `OK`
- bitlab/plugins/jquerymask `OK`
- bitlab/plugins/bstimeout `OK`
- bitlab/plugins/bstour `OK`
- bitlab/plugins/ckeditor `OK`
- bitlab/plugins/gmap `OK`
- bitlab/plugins/map `OK`
- bitlab/plugins/maxlength `OK`
- bitlab/plugins/moment `OK`
- bitlab/plugins/select2 `OK`
- bitlab/plugins/sparkline `OK`
- bitlab/plugins/typeahead `OK`
- bitlab/plugins/circles `OK`
- bitlab/plugins/cropper
- bitlab/plugins/dropzone
- bitlab/plugins/fileupload
- bitlab/plugins/flip
- bitlab/plugins/footable
- bitlab/plugins/fullcalendar
- bitlab/plugins/globalize
- bitlab/plugins/holder
- bitlab/plugins/imagezoom
- bitlab/plugins/jquerydial
- bitlab/plugins/jqueryflot
- bitlab/plugins/jquery.spin
- bitlab/plugins/ladda
- bitlab/plugins/lazyline
- bitlab/plugins/magnific
- bitlab/plugins/markdown
- bitlab/plugins/nestable
- bitlab/plugins/nprogress
- bitlab/plugins/mixitup
- bitlab/plugins/pnotify
- bitlab/plugins/slick
- bitlab/plugins/summernote
- bitlab/plugins/tabdrop
- bitlab/plugins/tagmanager
- bitlab/plugins/tagsinput
- bitlab/plugins/waypoints
- bitlab/plugins/xeditable
