require 'rails'

require_relative '../../../app/helpers/navbar_menu_helper.rb'
require_relative '../../../app/helpers/sidebar_menu_helper.rb'
require_relative '../../../app/helpers/message_helper.rb'

module Twitter
  module Bootstrap
    module Rails
      class Engine < ::Rails::Engine
        initializer 'bootstrap-admin.setup',
          :after => 'less-rails.after.load_config_initializers',
          :group => :all do |app|
            if defined?(Less)
              app.config.less.paths << File.join(config.root, 'vendor', 'toolkit')
            end
          end

        initializer 'bootstrap-admin.setup_helpers' do |app|
          # app.config.to_prepare do
          #   ActionController::Base.send :include, Breadcrumbs
          # end

          [
            FormStyleHelper,
            NavbarMenuHelper,
            SidebarMenuHelper,
            MessageHelper
          ].each do |h|
            app.config.to_prepare do
              ActionController::Base.send :helper, h
            end
          end
          # ActionView::Helpers::FormBuilder.send :include, FormErrorsHelper
        end
      end
    end
  end
end
