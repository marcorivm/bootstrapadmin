module GenericListHelper
  def delete_button(object, icon_class = 'glyphicons glyphicons-remove_2', message = nil)
    message ||= "El #{object.model_name.human} se eliminará, ¿deseas continuar?"
    link_to polymorphic_path(object), method: :delete,
      data: { confirm: message }, class: 'btn btn-danger' do
      content_tag :span, nil, class: icon_class,
        aria: { hidden: true }, title: 'Eliminar'
    end
  end

  def custom_button(object, options = nil)
    options[:icon_class] ||= "glyphicon-retweet"
    show = (object.class.method_defined? :show?)? object.send(:show?) : true
    link_to eval(options[:path]), method: options[:method], data: options[:data],
      remote: options[:remote], class: options[:class] do
        content_tag :span, nil, class: options[:icon_class],
          aria: { hidden: true }, title: options[:text]
    end if show
  end

  def edit_button(object, icon_class = 'glyphicons glyphicons-pencil')
    link_to edit_polymorphic_path(object), class: 'btn btn-success' do
      content_tag :span, nil, class: icon_class, aria: { hidden: true }, title: 'Editar'
    end
  end

  def view_button(object, icon_class = 'glyphicon glyphicon-eye-open')
    link_to polymorphic_path(object), class: 'btn btn-info' do
      content_tag :span, nil, class: icon_class, aria: { hidden: true }, title: 'Ver'
    end
  end

  def sortable(column)
    title = object_class.human_attribute_name(column)
    column = column.to_s
    css = column == sort_column ? "current #{sort_direction}" : nil
    direction = !css.nil? && sort_direction == 'asc' ? 'desc' : 'asc'
    link_to title, uri_params({sort: column, direction: direction}), class: css
  end

  def highchart(chart_params)
    msg_error = "Favor de importar highcharts"
    content_tag :div, msg_error, class: 'highcharts', data: chart_params
  end
end
