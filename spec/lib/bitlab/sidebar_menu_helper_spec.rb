# encoding: utf-8
require 'spec_helper'
require 'action_view'
require 'active_support'
require_relative '../../../app/helpers/sidebar_menu_helper'

include ActionView::Helpers
include ActionView::Context
include SidebarMenuHelper

describe NavbarMenuHelper, type: :helper do

  it 'should return an icon link for sidebar' do
    expect(sidebar_icon_link('TITLE', 'LINK', 'fa fa-calendar', 'BADGE')
    .gsub(/\s/, '').downcase).to eql(SIDEBAR_ICON_LINK.gsub(/\s/, '').downcase)
  end

  it 'should return an image link for sidebar' do
    expect(sidebar_image_link('TITLE', 'LINK', 'navigator/user.png', 'BADGE')
    .gsub(/\s/, '').downcase).to eql(SIDEBAR_IMAGE_LINK.gsub(/\s/, '').downcase)
  end

  it  'should return a sidebar_icon_accordion' do
    expect(sidebar_icon_accordion('TITLE', 'fa fa-columns'){}.gsub(/\s/, '').downcase)
    .to eql(SIDEBAR_ICON_ACCORDION.gsub(/\s/, '').downcase)
  end

  it  'should return a sidebar_image_accordion' do
    expect(sidebar_image_accordion('TITLE', 'fa fa-columns'){}.gsub(/\s/, '').downcase)
    .to eql(SIDEBAR_IMAGE_ACCORDION.gsub(/\s/, '').downcase)
  end

  it  'should return a sidebar label' do
    expect(sidebar_label('Menu').gsub(/\s/, '').downcase)
    .to eql(SIDEBAR_LABEL.gsub(/\s/, '').downcase)
  end

  it 'should return a sidebar' do
    expect(sidebar(nil){sidebar_menu {}}.gsub(/\s/, '').downcase)
    .to eql(SIDEBAR_BASIC.gsub(/\s/, '').downcase)
  end

end

SIDEBAR_BASIC = <<-HTML
  <aside id="sidebar_left" class="nano nano-light affix has-scrollbar">
  <div class="sidebar-left-content nano-content" tabindex="0"
  style="margin-right: -15px;"><ul class="nav sidebar-menu">           </ul>
  </div><div class="nano-pane"><div class="nano-slider"
  style="height: 210px; transform: translate(0px, 0px);"></div></div></aside>
HTML
SIDEBAR_LABEL = <<-HTML
  <li class="sidebar-label pt20">MENU</li>
HTML
SIDEBAR_ICON_LINK = <<-HTML
  <li><a href="LINK"><span class="fa fa-calendar"></span>
  <span class="sidebar-title">TITLE</span><span class="sidebar-title-tray">
  <span class="label label-xs bg-primary">BADGE</span></span></a></li>
HTML
SIDEBAR_IMAGE_LINK = <<-HTML
  <li><a href="LINK"><img src="/images/navigator/user.png" alt="user"/>
  <span class="sidebar-title">TITLE</span><span class="sidebar-title-tray">
  <span class="label label-xs bg-primary">BADGE</span></span></a></li>
HTML
SIDEBAR_ICON_ACCORDION = <<-HTML
  <li><a class="accordion-toggle" href="#"><span class="fa fa-columns"></span>
  <span class="sidebar-title">TITLE</span><span class="sidebar-title-tray">
  <span class="label label-xs bg-primary"></span></span></a><ul class="nav
  sub-nav">                                                  </ul></li>
HTML
SIDEBAR_IMAGE_ACCORDION = <<-HTML
  <li><a class="accordion-toggle" href="#"><img src="/images/fafa-columns"
  alt="fafacolumns"/><span class="sidebar-title">TITLE</span><span
  class="sidebar-title-tray"><span class="label label-xs bg-primary"></span>
  </span></a><ul class="nav sub-nav">                                 </ul></li>
HTML
SIDEBAR_ACCORDION_LINK = <<-HTML
  <li><a href="LINK">TITLE</a></li>
HTML
SIDEBAR_STATUS = <<-HTML
  <li class="sidebar-stat"><a href="#projectOne" class="fs11"><span
  class="fa fa-dropbox text-warning"></span><span class="sidebar-title
  text-muted">Bandwidth</span><span class="pull-right mr20 text-muted">70%
  </span><div class="progress progress-bar-xs mh20"><div class="progress-bar
  progress-bar-warning" role="progressbar" aria-valuenow="45" aria-valuemin="0"
  aria-valuemax="100" style="width: 70%"><span class="sr-only">70% COMPLETO
  </span></div></div></a></li>
HTML
